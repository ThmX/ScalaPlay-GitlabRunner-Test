# http://tbd
FROM java:8-jdk
MAINTAINER Thomas Denoréaz <thomas.denoreaz@gmail.com>

# Install Activator
RUN apt-get update -y && apt-get install -y curl unzip
RUN curl -O https://downloads.typesafe.com/typesafe-activator/1.3.10/typesafe-activator-1.3.10.zip
RUN unzip typesafe-activator-1.3.10.zip -d / && rm typesafe-activator-1.3.10.zip && chmod a+x /activator-dist-1.3.10/bin/activator
ENV PATH $PATH:/activator-dist-1.3.10/bin

# Open Ports
EXPOSE 9000 9000

ADD . /app
VOLUME /app
WORKDIR /app

#CMD ["activator", "run", "-Dconfig.resource=application-dev.conf"]
#CMD ["activator", "run"]